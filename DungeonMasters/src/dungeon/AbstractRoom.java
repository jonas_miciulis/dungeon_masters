package dungeon;

import gameEngine.GameEngine;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.util.Random;
import Display.*;

public abstract class AbstractRoom implements Focusable{
	private int height;
	private int width;
	private int doors[];
	private int doorNumber;
	private boolean isEndRoom;
	private Dimension position;
	private Panel panels[][];
	
        public Dimension getPosition(){return this.position;}
        public int[] getDoors(){return this.doors;}
 	public void setPanels(Panel[][] room){this.panels = room;}    
        public int getDoorNumber(){return this.doorNumber;}
	public int getHeight(){return this.height;}
	public int getWidth(){return this.width;}
        
	protected abstract void generateRoom();
	public abstract boolean isEndRoom();
	public abstract boolean setEndRoom();
	public abstract void enterRoom(int direction);
	public abstract String getName();
	public abstract String displayString();
	public abstract boolean update(KeyEvent e, boolean focused);
	public abstract String eventString();
	public abstract void open();
        public abstract boolean isNil();
}